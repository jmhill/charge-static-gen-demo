export default props => {
  const { title, children, ...rest } = props;
  return (
    <html>
      <head>
        <title>{title}</title>
      </head>

      <body>
        <h1>{title}</h1>
        <div>{children}</div>
      </body>
      <footer>{<div>{JSON.stringify(rest)}</div>}</footer>
    </html>
  );
};
