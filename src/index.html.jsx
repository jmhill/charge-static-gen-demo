import Layout from "./components/Layout";

export default props => {
  return (
    <Layout title="Welcome" {...props}>
      <div>Hello Charge!</div>
    </Layout>
  );
};
